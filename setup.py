#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name="autovpn",
    version="0.3",
    description="Automated CA management (web frontend).",
    author="Ale",
    author_email="ale@incal.net",
    url="https://git.autistici.org/ai/autovpn",
    license="MIT",
    packages=find_packages(),
    platforms=["any"],
    install_requires=[
        "pyOpenSSL", "Flask", "itsdangerous", "autoca"
    ],
    zip_safe=False,
    entry_points={
        "console_scripts": [
            "vpnacct=autovpn.acct:main",
        ],
    },
    package_data={
        "autovpn": ["templates/*", "static/*"],
    },
    dependency_links=[
        "git+https://git.autistici.org/ai/autoca.git#egg=autoca-0.3",
    ],
)
