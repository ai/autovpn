import tempfile
import shutil
import unittest
from flask import session
from autovpn import vpn_app


class VpnAppTest(unittest.TestCase):

    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()
        self.config = {
            'DEBUG': 'true',
            'SECRET_KEY': 'somesecret',
            'SIGNER_SECRET': 'abracadabra',
            'CACHE_DIR': self.tmpdir,
            'VPN_CA_ROOT': self.tmpdir,
            'VPN_CA_SUBJECT': {'CN': 'test CA', 'O': 'test'},
            'VPN_CA_BITS': 1024,
            'VPN_ENDPOINT': 'vpn.example.com',
            'VPN_SITE_URL': 'http://localhost:4000/',
            'FOOTER': '''
<p class="footer">
  built by <a href="https://www.autistici.org/">autistici.org</a>
</p>
''',
            'AUTH_ENABLE': True,
            'AUTH_FUNCTION': lambda x, y: (x and y and x == y),
            'TLS_AUTH_KEY': self.tmpdir + '/tlsauth.key',
            }

        self.app = vpn_app.make_app(self.config)

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def test_login_ok(self):
        with self.app.test_client() as c:
            rv = c.get('/login')
            csrf = session['_csrf']
            rv = c.post('/login', data={
                    '_csrf': csrf,
                    'username': 'admin',
                    'password': 'admin'},
                        follow_redirects=True)
            self.assertTrue('download of the ZIP file' in rv.data)

    def test_login_fail(self):
        with self.app.test_client() as c:
            rv = c.get('/login')
            csrf = session['_csrf']
            rv = c.post('/login', data={
                    '_csrf': csrf,
                    'username': 'user',
                    'password': 'wrong password'},
                        follow_redirects=True)
            self.assertFalse(session.get('dl_ok'))
            self.assertTrue('Authentication failed' in rv.data)

    def test_cert_dl(self):
        cn = 'testcn1234'
        t = self.app.signer.encode(cn)
        with self.app.test_client() as c:
            with c.session_transaction() as sess:
                sess['dl_ok'] = True
                sess['_csrf'] = 'csrf'
                sess['logged_in'] = True
            rv = c.get('/newcertdl?_csrf=csrf&t=' + t)
            self.assertEquals('200 OK', rv.status)
            self.assertEquals('application/zip', rv.content_type)
